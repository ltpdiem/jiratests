﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;

namespace JiraTestProject
{
    public class JiraHomePage : BasePage
    {
        public JiraHomePage(IWebDriver driver)
            : base(driver, "Agile Board - Atlassian JIRA")
        {
            //WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            //IWebElement LoginElement = wait.Until<IWebElement>((d) =>
            //{
            //    return d.FindElement(By.Id("user-options"));
            //});

            // Check that we're on the right page contain element
            if (!driver.FindElement(By.Id("user-options")).Displayed)
            {
                // Alternatively, we could navigate to the expected page
                throw new InvalidOperationException("This is not Home page");
            } 
        }

        #region Properties
        // The home page contains several HTML elements that will be represented as WebElements.
        // The locators for these elements should only be defined once.
        ByChained LoginButtonLocator = new ByChained(By.Id("user-options"),By.ClassName("login-link"));
        By CreateNewIssueLocator = By.Id("create_link");
        By SummaryLocator = By.Id("summary");
        By IssueTypeLocator = By.Id("issuetype-field");
        By SubmitCreateIssueLocator = By.Id("create-issue-submit");
        By IssuesButtonLocator = By.Id("find_link");
        #endregion

        public JiraLoginPage NavigateLoginPage()
        {
            // click on Log In link
           
            WebDriver.FindElement(LoginButtonLocator).Click();
            return new JiraLoginPage(WebDriver); 
        }

        // Open Create Issue page
        public JiraIssuesPage OpenCreateIssue()
        {
            // click on Create button
            WebDriver.FindElement(CreateNewIssueLocator).Click();

            // Return the current page object as this action doesn't navigate to a page represented by another PageObject
            return new JiraIssuesPage(WebDriver);
        }


        // Open Create Issue page
        public JiraHomePage OpenRecentIssues()
        {
            // click on Create button
            WebDriver.FindElement(IssuesButtonLocator).Click();

            // Return the current page object as this action doesn't navigate to a page represented by another PageObject
            return this;
        }

        
        /// <summary>
        /// Finds the new created issue id.
        /// </summary>
        /// <param name="sSummary">The issue summary text.</param>
        /// <returns>The matching id if exists, blank text if not exists
        public string FindNewIssueId(string sSummary)
        {
            string sIssueId = "";
            var listResult = WebDriver.FindElements(By.ClassName("issue-link"));
            if (listResult != null)
            {
                foreach(var item in listResult)
                {
                    string sDataIssueKey = item.GetAttribute("data-issue-key");
                    string sTitle = item.GetAttribute("title");
                    string sTitleSummary = sTitle.Substring(sDataIssueKey.Length + 1, sTitle.Length - sDataIssueKey.Length - 1);
                    if (sTitleSummary.Equals(sSummary))
                    {
                        sIssueId = item.GetAttribute("id");
                        break;
                    }
                        
                }
                
            }
            return sIssueId;

        }
    }
}
