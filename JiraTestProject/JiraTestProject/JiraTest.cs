﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

// Requires reference to WebDriver.Support.dll
using OpenQA.Selenium.Support.UI;

namespace JiraTestProject
{
    [TestClass]
    public class JiraTest
    {
        IWebDriver driver;

        [TestMethod]
        //Verify new issues can be created
        public void TestMethod1()
        {
            // Initialize
            string sBlankIssueId = "";
            string sNewIssueId = "";
            string sUsername = "ltpdiem@yahoo.com";
            string sPassword = "ltpdiem123";
            string sSummary = "Test new issue";
            string sIssueType = "Bug";
            Random Randomizer = new Random();
            sSummary = sSummary + " " + Randomizer.Next(999);

            // Create a new instance of the JiraHomePage.
            JiraHomePage HomePage = new JiraHomePage(driver);

            // Login and create new bug in current project
            HomePage.NavigateLoginPage()
                .Login(sUsername, sPassword)
                .OpenCreateIssue()
                .CreateNewIssue(sSummary, sIssueType);

            // verify new issue is created
            sNewIssueId = HomePage.OpenRecentIssues()
                            .FindNewIssueId(sSummary);
            
            Assert.AreNotEqual(sNewIssueId, sBlankIssueId, "New issue was created successful.");
       
            
        }

        [TestInitialize]
        public void TestInitialize()
        {
            // Navigate to public Jira Test project
            driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("https://jira.atlassian.com/browse/TST");

        }

        [TestCleanup]
        public void TestCleanup()
        {
            //Close the browser
            driver.Quit();
        }

    }
}
