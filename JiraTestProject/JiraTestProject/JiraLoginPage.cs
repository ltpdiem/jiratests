﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;

namespace JiraTestProject
{
    public class JiraLoginPage : BasePage
    {
       
        public JiraLoginPage(IWebDriver driver) : base(driver, "Log in to continue")
        {
            // Check that we're on the right page contain element
            if (!driver.FindElement(By.Id("login-submit")).Displayed)
            {
                // Alternatively, we could navigate to the expected page
                throw new InvalidOperationException("This is not Login page");
            } 
        }

        // The login page contains several HTML elements that will be represented as WebElements.
        // The locators for these elements should only be defined once.
        By UsernameLocator = By.Id("username");
        By PasswordLocator = By.Id("password");
        By LoginButtonLocator = By.Id("login-submit");
        By StayLoggedInLocator = By.Id("rememberMe");

        // The login page allows the user to type their username into the username field
        public JiraLoginPage TypeUsername(string sUsername) 
        {
            // This is the only place that "knows" how to enter a username
            WebDriver.FindElement(UsernameLocator).SendKeys(sUsername);

            // Return the current page object as this action doesn't navigate to a page represented by another PageObject
            return this;    
        }

        // The login page allows the user to type their password into the password field
        public JiraLoginPage TypePassword(string sPassword) 
        {
            // This is the only place that "knows" how to enter a password
            WebDriver.FindElement(PasswordLocator).SendKeys(sPassword);

            // Return the current page object as this action doesn't navigate to a page represented by another PageObject
            return this;    
        }

        // Do not stay logged in
        public JiraLoginPage DoNotStayLoggedIn()
        {
            // This is the only place that "knows" how to enter a Stay Logged In field 
            if (WebDriver.FindElement(StayLoggedInLocator).Selected)
                WebDriver.FindElement(StayLoggedInLocator).Click();
            // Return the current page object as this action doesn't navigate to a page represented by another PageObject
            return this; 
        }

        // The login page allows the user to submit the login form
        public JiraHomePage SubmitLogin() {
            // This is the only place that submits the login form and expects the destination to be the home page.
            // A seperate method should be created for the instance of clicking login whilst expecting a login failure. 
            WebDriver.FindElement(LoginButtonLocator).Submit();

            // Return a new page object representing the destination. Should the login page ever
            // go somewhere else (for example, a legal disclaimer) then changing the method signature
            // for this method will mean that all tests that rely on this behaviour won't compile.
            return new JiraHomePage(WebDriver);    
        }

        // The login page allows the user to submit the login form knowing that an invalid username and / or password were entered
        public JiraLoginPage SubmitLoginExpectingFailure() 
        {
            // This is the only place that submits the login form and expects the destination to be the login page due to login failure.
            WebDriver.FindElement(LoginButtonLocator).Submit();

            // Return a new page object representing the destination. Should the user ever be navigated to the home page after submiting a login with credentials 
            // expected to fail login, the script will fail when it attempts to instantiate the LoginPage PageObject.
            return new JiraLoginPage(WebDriver);   
        }

        // Conceptually, the login page offers the user the service of being able to "log into"
        // the application using a user name and password. 
        public JiraHomePage Login(string sUsername, string sPassword) 
        {
            // The PageObject methods that enter username, password & submit login have already defined and should not be repeated here.
            TypeUsername(sUsername);
            TypePassword(sPassword);
            DoNotStayLoggedIn();
            return SubmitLogin();
        }
    }
}
