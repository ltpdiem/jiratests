﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace JiraTestProject
{
    public class BasePage
    {
        #region Private
        private IWebDriver driver;
        #endregion
        
        
        public BasePage(IWebDriver driver)
        {
            
        }

        public BasePage(IWebDriver webdriver, string sTitle) 
        {
            this.driver = webdriver;

            Thread.Sleep(TimeSpan.FromSeconds(1));
            
            // Check that we're on the right page contain title
            if (!driver.Title.Contains(sTitle))
            {
                // Alternatively, we could navigate to the expected page
                throw new InvalidOperationException("This is not expected page");
            }
        }

        #region Public properties
        public IWebDriver WebDriver
        {
            get
            {
                if (driver == null)
                {
                    var firefoxProfile = new FirefoxProfile
                    {
                        AcceptUntrustedCertificates = true,
                        EnableNativeEvents = true
                    };

                    driver = new FirefoxDriver(firefoxProfile);
                }
                return driver;
            }

        }
        #endregion

    }
    
}