﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;

namespace JiraTestProject
{
    public class JiraIssuesPage : BasePage
    {
        public JiraIssuesPage(IWebDriver driver)
            : base(driver, "Atlassian JIRA")
        {
            
            // Check that we're on the right page contain element
            if (!driver.FindElement(By.Id("create-issue-submit")).Displayed)
            {
                // Alternatively, we could navigate to the expected page
                throw new InvalidOperationException("This is not Create Issues page");
            } 
        }


        #region Properties
        // The Create Issues page contains several HTML elements that will be represented as WebElements.
        // The locators for these elements should only be defined once.
        By SummaryLocator = By.Id("summary");
        By IssueTypeLocator = By.Id("issuetype-field");
        ByChained IssueTypeDropdownLocator = new ByChained(By.Id("issuetype-single-select"), By.ClassName("drop-menu"));
        By BugTypeLocator = By.ClassName("aui-list-item-li-bug");
        By SubmitCreateIssueLocator = By.Id("create-issue-submit");
        #endregion

        // The create issue page allows the user to type their summary into the summary field
        public JiraIssuesPage TypeSummary(string sSummary)
        {
            // This is the only place that "knows" how to enter a summary
            WebDriver.FindElement(SummaryLocator).SendKeys(sSummary);
            // Return the current page object as this action doesn't navigate to a page represented by another PageObject
            return this;
        }

        // The create issue page allows the user to type their issue type into the issue type field
        public JiraIssuesPage SelectIssueType(string sIssueType)
        {
            // This is the only place that "knows" how to enter a issue type
            if (!WebDriver.FindElement(IssueTypeLocator).Text.Equals(sIssueType)) 
            {
                WebDriver.FindElement(IssueTypeDropdownLocator).Click();
                switch (sIssueType)
                {
                    case "Bug":
                        WebDriver.FindElement(BugTypeLocator).Click();
                        break;
                    default:
                        WebDriver.FindElement(BugTypeLocator).Click();
                        break;
                }

                Thread.Sleep(TimeSpan.FromSeconds(1));
            }
            // Return the current page object as this action doesn't navigate to a page represented by another PageObject
            return this;
        }
        // The login page allows the user to submit the create issue form
        public JiraHomePage SubmitCreateIssue()
        {
            // This is the only place that submits the create issue form and expects the destination to be the home page.
            WebDriver.FindElement(SubmitCreateIssueLocator).Submit();
            Thread.Sleep(TimeSpan.FromSeconds(60));
            // Return the Home page
            return new JiraHomePage(WebDriver);
        }

        public JiraHomePage CreateNewIssue(string sSummary, string sIssueType)
        {
            // Select Bug type and enter Summary
            SelectIssueType(sIssueType);
            TypeSummary(sSummary);

            // Return the current page object as this action doesn't navigate to a page represented by another PageObject
            return SubmitCreateIssue();
        }

        
    }

    
}
